#pragma once

#include "EncodedCameraStream.hpp"

#include <FramedSource.hh>

class UsageEnvironment;

class H264EncodedDM365CameraFrameSource : public FramedSource, public StreamEventListner
{
protected:
   explicit H264EncodedDM365CameraFrameSource( UsageEnvironment &rEnvironment );

public:
   virtual ~H264EncodedDM365CameraFrameSource();
   void doGetNextFrame();
   void deliverFrame();

   void OnFrameReady();

   static H264EncodedDM365CameraFrameSource* createNew( UsageEnvironment &rEnvironment ); 
private:
   bool m_bFrameNeeded, m_bFrameUpdated, m_bFrameSent, m_bShouldSignalNewFrame;

   std::tr1::shared_ptr< EncodedCameraStream > m_pEncodedCameraStream;
   static EventTriggerId s_nEventTriggerId;
};

/* Static helper function to signal first frame */
void DeliverFrame0( void *pClientData );