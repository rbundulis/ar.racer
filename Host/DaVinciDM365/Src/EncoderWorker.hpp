#pragma once

#include "CameraWorker.hpp"
#include "Posix.hpp"

#include <ti/sdo/dmai/BufTab.h>
#include <ti/sdo/dmai/ce/Venc1.h>

class EncoderEventListener
{
public:
   virtual void OnFrameEncoded() = 0;
};

class EncoderWorker : public posix::Thread< EncoderWorker >
{
public:
	EncoderWorker( Engine_Handle engine, CameraWorker &cameraWorker, BufTab_Handle sharedBuffers, VIDENC1_Params &EncoderParameters );
	virtual ~EncoderWorker();
    
    bool IsEncodedFrameAvailable();
    void GetEncodedFrame( unsigned char* rpBuffer, size_t &rnUsedBufferSize, size_t &rnActualBufferSize );
	int Run();
	void SetListener( EncoderEventListener *pListener );
private:
	Venc1_Handle          m_Encoder;
    BufTab_Handle         m_SharedBuffers;
    Buffer_Handle         m_EncodedFrame;
    VIDENC1_DynamicParams m_DynamicEncoderParameters;
	CameraWorker         &m_CameraWorker;
	posix::Mutex           m_ListenerMutex;
	EncoderEventListener *m_pListener;
	unsigned int          m_nFrameNumber;
	bool                  m_bCanEncodeNextFrame;
	bool                  m_bHasAvailableFrame;
};