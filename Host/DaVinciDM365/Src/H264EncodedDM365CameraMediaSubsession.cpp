#include "H264EncodedDM365CameraMediaSubsession.hpp"
#include "H264EncodedDM365CameraFrameSource.hpp"
#include "Common.hpp"

#include <BasicUsageEnvironment.hh>
#include <H264VideoRTPSink.hh>
#include <H264VideoStreamDiscreteFramer.hh>

H264EncodedDM365CameraMediaSubsession::H264EncodedDM365CameraMediaSubsession(UsageEnvironment &env) : OnDemandServerMediaSubsession(env, true)
{

}

FramedSource* H264EncodedDM365CameraMediaSubsession::createNewStreamSource(unsigned int, unsigned int&estBitrate)
{
	TRACE( "enter\n" );
	estBitrate = 2000; // kbps, estimate
	H264EncodedDM365CameraFrameSource* frameSource = H264EncodedDM365CameraFrameSource::createNew(envir());
	if (frameSource == NULL)
	{
		LOG() << "ERROR: failed to create frame source\n";
		return NULL;
	}
	return H264VideoStreamDiscreteFramer::createNew(envir(), frameSource);
}

RTPSink* H264EncodedDM365CameraMediaSubsession::createNewRTPSink(Groupsock* rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic, FramedSource* inputSource)
{
	TRACE( "enter\n" );
	return H264VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic);
}

H264EncodedDM365CameraMediaSubsession* H264EncodedDM365CameraMediaSubsession::createNew(UsageEnvironment &env)
{
	TRACE( "enter\n" );
	return new H264EncodedDM365CameraMediaSubsession(env);
}