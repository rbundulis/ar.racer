#pragma once

#include <OnDemandServerMediaSubsession.hh>

#include <string>
#include <memory>

class UsageEnvironment;

class H264EncodedDM365CameraMediaSubsession : public OnDemandServerMediaSubsession 
{
protected:	
	explicit H264EncodedDM365CameraMediaSubsession(UsageEnvironment &env);

	FramedSource* createNewStreamSource(unsigned int, unsigned int& estBitrate);
	RTPSink* createNewRTPSink(Groupsock* rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic, FramedSource* inputSource);

public:	
	static H264EncodedDM365CameraMediaSubsession* createNew(UsageEnvironment &env);
};