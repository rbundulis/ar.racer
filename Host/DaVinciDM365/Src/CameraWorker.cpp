#include "CameraWorker.hpp"
#include "Common.hpp"

#include <ti/sdo/dmai/Time.h>

#include <stdexcept>
#include <cstdlib>

CameraWorker::CameraWorker( BufTab_Handle sharedBuffers, Capture_Attrs &captureAttributes ) : m_SharedBuffers( sharedBuffers ), m_pListener( NULL )
{
   TRACE( "enter\n" );
   m_Capture = Capture_create( m_SharedBuffers, &captureAttributes );
   if(m_Capture == NULL )
   {
      LOG() << "ERROR: Failed to create capture device\n";
      throw std::runtime_error( "failed to create capture device" );
   }
   TRACE( "leave\n" );
}

CameraWorker::~CameraWorker()
{
	TRACE( "enter\n" );
	Stop();
	if( m_Capture )
	{
	   Capture_delete( m_Capture );
	}
	TRACE( "leave\n" );
}

Buffer_Handle CameraWorker::GetLastFrame()
{
   TRACE( "enter\n" );
   static int nIndex = 0;
   Buffer_Handle buffer = NULL;
   posix::AutoMutexLocker oLocker( m_FrameLocker );
   int nCount = 0, nBufferCount = BufTab_getNumBufs( m_SharedBuffers );
   {
     
      while( ( buffer == NULL ) && nCount < nBufferCount)
      {    
         buffer = Capture_get_buf_used( m_Capture, nIndex++ );
         if( nIndex >= nBufferCount)
         {
            nIndex = 0;
         }
         nCount++;
      }
  }
  if( buffer == NULL )
  {
    LOG() << "No frame read\n";
  }
   else
   {
      m_FrameTaken.Signal();
      PROFILE( "Frame taken from camera queue\n" );
   }
   return buffer;
}

int CameraWorker::Run()
{
   TRACE( "enter\n" );
   Buffer_Handle dummyBuffer = NULL;
   {
      posix::AutoMutexLocker oLocker( m_FrameLocker );
      if( Capture_dqbuf( m_Capture, &dummyBuffer ) < 0 )
      {
         LOG() << "ERROR: Failed to get capture buffer\n";
      }
   }
   Time_Handle hTime = NULL;
   Time_Attrs tAttrs = Time_Attrs_DEFAULT;
   hTime = Time_create(&tAttrs);
   bool bGotFirstFrame = false;
   while( !m_bShouldStop )
   {
       CheckForPause();
      {
         posix::AutoMutexLocker oLocker( m_FrameLocker );
         if( Capture_qbuf( m_Capture ) < 0 )
         {
            LOG() << "ERROR: Failed to put capture buffer\n";
         }
         if( Capture_dqbuf( m_Capture, &dummyBuffer ) < 0 )
         {
            LOG() << "ERROR: Failed to get capture buffer\n";
         }
	     else
  	   {
         PROFILE( "New frame from camera\n" );
         if( bGotFirstFrame == false )
  		    {
  			   bGotFirstFrame = true;
  			   {
  			       posix::AutoMutexLocker oLocker( m_ListenerMutex );
	  			   if( m_pListener != NULL )
	  			   {
                PROFILE( "Signalling first frame\n" );
                m_pListener->OnFirstFrameReceived();
                m_FrameTaken.Wait( m_FrameLocker );
	  			   }
	  		   }
  			}
         }
  	  }
      UInt32 time;
      Time_delta(hTime, &time);
      if(time < 40000)
      {
    	   PROFILE( "Camera thread is sleeping " << static_cast< int >( 40000 - time ) << "us\n" );
         usleep(40000 - time);  /*  40ms which is 25 fps  */
      }
      Time_reset(hTime);
   }
   return EXIT_SUCCESS;
   TRACE( "leave\n" );
}

void CameraWorker::SetListener( CameraEventListener *pListener )
{
	posix::AutoMutexLocker oLocker( m_ListenerMutex );
	m_pListener = pListener;
}
