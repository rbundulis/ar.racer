#pragma once

#include "Posix.hpp"

template< typename T >
class Logger;

#include "Singleton.hpp"

#include <sys/time.h>



#ifndef LOGGER_TYPE
#error "To use logger the LOGGER_TYPE macro must be defined as the underlying log sink type"
#endif

#include <stdio.h>

inline std::string GetCurrentDateTime()
{
   /* Format: [dd-mm-yy hh:mm:ss.mmmm], size 24 + null terminator */
   char pDateTimeBuffer[ 25 ];
   time_t currentTime = time( NULL );
   tm localTime;
   localtime_r( &currentTime, &localTime );
   timeval timeOfDay;
   gettimeofday( &timeOfDay, NULL );
   sprintf( pDateTimeBuffer, "[%02d-%02d-%d %02d:%02d:%02d.%03d]", localTime.tm_mday, localTime.tm_mon + 1, 1900 + localTime.tm_year, localTime.tm_hour, localTime.tm_min, localTime.tm_sec, static_cast< int >( timeOfDay.tv_usec / 1000 ) );
   return std::string( pDateTimeBuffer );
}

template< typename T >
class Logger : public Singleton< Logger< T > >
{
friend class Singleton< Logger< T > >;
public:
	void SetSink( T* pLogSink )
	{
		posix::AutoMutexLocker oLocker( m_Mutex );
		m_pLogSink = pLogSink;
	}

	template< typename K >
	Logger& operator << ( K k )
	{
		posix::AutoMutexLocker oLocker( m_Mutex );
		if( m_pLogSink )
		{
			*m_pLogSink << k;
		}
		return *this;
	}

private:
	Logger() : m_pLogSink( NULL ) {}
    
    posix::Mutex m_Mutex;
	T* m_pLogSink;
};

#define INIT_LOG() std::tr1::shared_ptr< Logger< LOGGER_TYPE > > LoggerInstance = Logger< LOGGER_TYPE >::GetInstance()
#define SET_LOG_SINK( x ) if( Logger< LOGGER_TYPE >::GetInstance() ) { Logger< LOGGER_TYPE >::GetInstance()->SetSink( x ); }
#define LOG() *Logger< LOGGER_TYPE >::GetInstance() << "LOG: "
#define LOG_TIME() Logger< LOGGER_TYPE >::GetInstance() << GetCurrentDateTime().c_str()

#ifdef ENABLE_TRACE
#define TRACE( x ) *Logger< LOGGER_TYPE >::GetInstance() << GetCurrentDateTime().c_str() << " TRACE: " << __PRETTY_FUNCTION__  << "@" << __FILE__ << ":" << __LINE__ << " " <<  x 
#else
#define TRACE( x )
#endif

#ifdef ENABLE_PROFILE
#define PROFILE( x ) *Logger< LOGGER_TYPE >::GetInstance() << GetCurrentDateTime().c_str() << " PROFILE: " << __PRETTY_FUNCTION__  << "@" << __FILE__ << ":" << __LINE__ << " " <<  x 
#else
#define PROFILE( x )
#endif
