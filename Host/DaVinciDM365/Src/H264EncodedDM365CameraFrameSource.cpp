#include "Common.hpp"
#include "H264EncodedDM365CameraFrameSource.hpp"

/* Initialization of static class members */
EventTriggerId    H264EncodedDM365CameraFrameSource::s_nEventTriggerId = 0;

H264EncodedDM365CameraFrameSource::H264EncodedDM365CameraFrameSource( UsageEnvironment &rEnvironment ) : FramedSource( rEnvironment ), m_bFrameNeeded( true ), 
   m_bFrameUpdated( false ), m_bFrameSent( false ), m_bShouldSignalNewFrame( true ), m_pEncodedCameraStream( EncodedCameraStream::GetInstance() )
{
   TRACE( "enter\n" );
   m_pEncodedCameraStream->AddListener( this );
   if( s_nEventTriggerId == 0 ) 
   {
      s_nEventTriggerId = rEnvironment.taskScheduler().createEventTrigger( &DeliverFrame0 );
   }
   TRACE( "leave\n" );
}

H264EncodedDM365CameraFrameSource::~H264EncodedDM365CameraFrameSource()
{
   TRACE( "enter\n" );
   envir().taskScheduler().deleteEventTrigger( s_nEventTriggerId );
   m_pEncodedCameraStream->RemoveListener( this );
   s_nEventTriggerId = 0;
   TRACE( "leave\n" );
}

void H264EncodedDM365CameraFrameSource::doGetNextFrame()
{
   TRACE( "enter\n" );
   if( m_pEncodedCameraStream->IsEncodedFrameAvailable() == true )
   {
      deliverFrame();
   }
   else
   {
      m_bShouldSignalNewFrame = true;
   }
   TRACE( "leave\n" );
}

H264EncodedDM365CameraFrameSource* H264EncodedDM365CameraFrameSource::createNew( UsageEnvironment &rEnvironment )
{
   return new H264EncodedDM365CameraFrameSource( rEnvironment );
}

void DeliverFrame0( void *pClientData ) 
{
   TRACE( "enter\n" );
   reinterpret_cast< H264EncodedDM365CameraFrameSource* >( pClientData )->deliverFrame();
   TRACE( "leave\n" );
}

void H264EncodedDM365CameraFrameSource::deliverFrame() 
{
   TRACE( "enter\n" );
   size_t nActualBufferSize, nOurBufferSize = fMaxSize;
   m_pEncodedCameraStream->GetEncodedFrame( fTo ,nOurBufferSize, nActualBufferSize );
   fFrameSize = nOurBufferSize;
   if( nActualBufferSize > nOurBufferSize )
   {
      fNumTruncatedBytes = nActualBufferSize - nOurBufferSize;
   }
   gettimeofday(&fPresentationTime, NULL);
   FramedSource::afterGetting(this);
   TRACE( "leave\n" );
}

void H264EncodedDM365CameraFrameSource::OnFrameReady()
{
   TRACE( "enter\n" );  
   if( m_bShouldSignalNewFrame )
   {
        m_bShouldSignalNewFrame = false;
      envir().taskScheduler().triggerEvent( s_nEventTriggerId, this );
   }
   TRACE( "leave\n" );
}