#pragma once

#include "Singleton.hpp"
#include "CameraWorker.hpp"
#include "EncoderWorker.hpp"
#include "Posix.hpp"

#include <vector>

#include <ti/sdo/dmai/ce/Venc1.h>

class StreamEventListner
{
public:
	virtual void OnFrameReady() = 0;
};

struct CameraStreamParameters
{
   unsigned int nWidth, nHeight, nBitrate, nGOPSize;
};

class EncodedCameraStream : public Singleton< EncodedCameraStream >, public CameraEventListener, public EncoderEventListener
{
public:
   EncodedCameraStream();
   virtual ~EncodedCameraStream();

   void AddListener( StreamEventListner *pListener );
   void GetEncodedFrame( unsigned char* rpBuffer, size_t &rnUsedBufferSize, size_t &rnActualBufferSize );
   bool IsEncodedFrameAvailable();
   void OnFrameEncoded();
   void OnFirstFrameReceived();
   void RemoveListener( StreamEventListner *pListener );

private:
   std::auto_ptr< CameraWorker > m_pCameraWorker;
   std::auto_ptr< EncoderWorker > m_pEncoderWorker;
   std::vector< StreamEventListner* > m_Listeners;
   posix::Mutex m_ListenerMutex;
   Engine_Handle m_Engine;
   BufTab_Handle m_SharedBuffers;
   bool m_bFirstRun;
};