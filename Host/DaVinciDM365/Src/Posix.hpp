#pragma once

#include <pthread.h>
#include <stdexcept>
#include <time.h>
#include <errno.h>

#include <string.h>
#include <iostream>

namespace posix
{

class Mutex
{
public:
   Mutex()
   {
#ifdef DEBUG_MUTEXES
      pthread_mutexattr_t mutexAttributes;
      pthread_mutexattr_init( &mutexAttributes );
      pthread_mutexattr_settype( &mutexAttributes, PTHREAD_MUTEX_ERRORCHECK );
      pthread_mutex_init( &m_Mutex, &mutexAttributes );
#else    
      pthread_mutex_init( &m_Mutex, NULL );
#endif      

   }

   ~Mutex()
   {
      pthread_mutex_destroy( &m_Mutex );
   }

   void Lock()
   {
#ifdef DEBUG_MUTEXES
      int nLockResult = pthread_mutex_lock( &m_Mutex );
      if( nLockResult )
      {
         std::cerr << "ERROR: Could not lock mutex " << this << ": " << strerror( nLockResult ) << "!\n";
      }
#else
      pthread_mutex_lock( &m_Mutex );
#endif      
   }
   
   void Unlock()
   {
#ifdef DEBUG_MUTEXES
      int nUnlockResult = pthread_mutex_unlock( &m_Mutex );
      if( nUnlockResult )
      {
         std::cerr << "ERROR: Could not unlock mutex " << this << ": " << strerror( nUnlockResult ) << "!\n";
      }
#else
      pthread_mutex_unlock( &m_Mutex );
#endif     
   }

   operator pthread_mutex_t *()
   {
      return &m_Mutex;
   }
  
private:
  pthread_mutex_t m_Mutex;
};

class AutoMutexLocker
{
public:
   AutoMutexLocker( Mutex &mutex ) : m_Mutex( mutex )
   {
      m_Mutex.Lock();
   }

   ~AutoMutexLocker()
   {
      m_Mutex.Unlock();
   }
private:
   Mutex &m_Mutex;
};

class Condition
{
public:
   Condition()
   {
      pthread_cond_init( &m_Condition, NULL );
   }

   ~Condition()
   {
      pthread_cond_destroy( &m_Condition );
   }

   void Wait( Mutex &mutex )
   {
      pthread_cond_wait( &m_Condition, mutex );
   }

   bool Wait( Mutex &mutex, const timespec &Timeout )
   {
       if( pthread_cond_timedwait( &m_Condition, mutex, &Timeout ) == ETIMEDOUT )
       {
          return false;
       }
       return true;
   }

   void Signal()
   {
      pthread_cond_signal( &m_Condition );
   }

private:
   pthread_cond_t m_Condition;
};

template< typename T >
class Thread;

template< typename T >
void *StartThread( void* pInstance )
{
   T *pT = static_cast< T* >( pInstance );
   pthread_exit( reinterpret_cast< void* >( pT->Run() ) );
}

template< typename T >
class Thread
{
public:
   Thread() : m_bShouldStop( false ), m_bStarted( false ), m_bPaused( false )
   {
   }

   virtual ~Thread()
   {
      Stop();
   }
   
   void Pause()
   {
      m_ResumeMutex.Lock();
      m_bPaused = true;
      m_ResumeMutex.Unlock();
   }

   void Resume()
   {
      m_ResumeMutex.Lock();
      m_bPaused = false;
      m_ResumeCondition.Signal();
      m_ResumeMutex.Unlock();
   }

   virtual int Run() = 0;
   
   bool IsStarted()
   {
      return m_bStarted;
   }

   void Start()
   {
      AutoMutexLocker oLocker( m_Mutex );
      if( pthread_create( &m_ThreadId, NULL,  &StartThread< T >, this ) )
      {
          throw std::runtime_error( "failed to start thread" );
      }
      m_bStarted = true;
   }

   void Stop()
   {
      AutoMutexLocker oLocker( m_Mutex );
      if( m_bStarted == true )
      {
         m_bShouldStop = true;
         pthread_cancel( m_ThreadId );
         pthread_join( m_ThreadId, 0 );
         m_bStarted = false;  
      }
   }
   
protected:
   bool m_bShouldStop;

   void CheckForPause()
   {
      m_ResumeMutex.Lock();
      while( m_bPaused )
      {
         m_ResumeCondition.Wait( m_ResumeMutex );
      }
      m_ResumeMutex.Unlock();
   }

private:
   Thread( const Thread& OtherThread ) {}

   Mutex m_Mutex;
   bool m_bStarted;
   bool m_bPaused;
   pthread_t m_ThreadId;
   Condition m_ResumeCondition;
   Mutex m_ResumeMutex;
};

}
