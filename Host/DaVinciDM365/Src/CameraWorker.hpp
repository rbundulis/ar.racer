#pragma once

#include "Posix.hpp"

#include <ti/sdo/dmai/Capture.h>
#include <ti/sdo/dmai/BufTab.h>

class CameraEventListener
{
public:
   virtual void OnFirstFrameReceived() = 0;
};

class CameraWorker : public posix::Thread< CameraWorker >
{
public:
	CameraWorker( BufTab_Handle sharedBuffers, Capture_Attrs &captureAttributes );
    ~CameraWorker();

    Buffer_Handle GetLastFrame();
	int Run();
	void SetListener( CameraEventListener *pListener );
private:
	posix::Mutex         m_FrameLocker;
	Capture_Handle       m_Capture;
	BufTab_Handle        m_SharedBuffers;
	posix::Mutex         m_ListenerMutex;
	posix::Condition     m_FrameTaken;
	CameraEventListener *m_pListener;
};

