#include "EncoderWorker.hpp"
#include "Common.hpp"

#include <ti/sdo/dmai/Time.h>

#include <string.h>

#include <cstdlib>

/* Align buffers to this cache line size (in bytes)*/
#define BUFSIZEALIGN            128

/* The input buffer height restriction */
#define CODECHEIGHTALIGN       16

EncoderWorker::EncoderWorker(  Engine_Handle engine, CameraWorker &cameraWorker,  BufTab_Handle sharedBuffers, VIDENC1_Params &encoderParameters  ) : m_SharedBuffers( sharedBuffers ),
   m_DynamicEncoderParameters( Venc1_DynamicParams_DEFAULT ), m_CameraWorker( cameraWorker ), m_nFrameNumber( 0 ), m_bCanEncodeNextFrame( true ), m_bHasAvailableFrame( false )
{
	BufferGfx_Attrs gfxAttrs = BufferGfx_Attrs_DEFAULT;
	Buffer_Attrs bAttrs = Buffer_Attrs_DEFAULT;
	m_DynamicEncoderParameters.targetBitRate      = encoderParameters.maxBitRate;
	m_DynamicEncoderParameters.inputWidth         = encoderParameters.maxWidth;
	m_DynamicEncoderParameters.inputHeight        = encoderParameters.maxHeight;

	    /* Create the video encoder */
	    char codec[8];
	    strcpy(codec, "h264enc");
	    m_Encoder = Venc1_create( engine, codec, &encoderParameters, &m_DynamicEncoderParameters );
	    if( m_Encoder == NULL )
	    {
	        LOG() << "ERROR: Failed to create h264 video encoder\n";
	        return;
	    }
	    /* Ask the codec how much input data it needs */
	    /* Ask the codec how much space it needs for output data */
	    Int inBufSize = Venc1_getInBufSize( m_Encoder ), outBufSize = Venc1_getOutBufSize( m_Encoder );

	    /* Align buffers to cache line boundary */    
	    gfxAttrs.bAttrs.memParams.align = bAttrs.memParams.align = BUFSIZEALIGN; 
	    gfxAttrs.dim.width      =  encoderParameters.maxWidth;
	    gfxAttrs.dim.height     = encoderParameters.maxHeight;
	    gfxAttrs.dim.height = Dmai_roundUp(gfxAttrs.dim.height, CODECHEIGHTALIGN);
	    gfxAttrs.dim.lineLength = BufferGfx_calcLineLength(encoderParameters.maxWidth, ColorSpace_YUV420PSEMI);
	    gfxAttrs.colorSpace     = ColorSpace_YUV420PSEMI;
	    
	    if (inBufSize < 0) {
	        LOG() << "ERROR: Failed to calculate buffer attributes\n";
	        return;
	    }


	    /* Set input buffer table */
	    Venc1_setBufTab( m_Encoder, m_SharedBuffers );

	    /* Create the output buffer for encoded video data */
	    m_EncodedFrame = Buffer_create(Dmai_roundUp(outBufSize, BUFSIZEALIGN), &bAttrs);
	    if (m_EncodedFrame == NULL) {
	       LOG() << "ERROR: Failed to create contiguous buffer\n";
	       return;
	    }
}

EncoderWorker::~EncoderWorker()
{
   TRACE( "enter\n");
   Stop();
   if (m_EncodedFrame)
   {
      Buffer_delete(m_EncodedFrame);
   }
   if (m_Encoder) 
   {
      Venc1_delete(m_Encoder);
   }
   TRACE( "leave\n");  
}

int EncoderWorker::Run()
{
	TRACE( "enter\n" );
    Time_Handle hTime = NULL;
    Time_Attrs tAttrs = Time_Attrs_DEFAULT;
    hTime = Time_create(&tAttrs);
    bool bFrameEncoded;
    while( !m_bShouldStop )
    {
      CheckForPause();
    	bFrameEncoded = false;
    	Buffer_Handle lastFrame = m_CameraWorker.GetLastFrame();
       	if( lastFrame == NULL)
       	{
           PROFILE( "Frame queue is empty\n" );
        }
        else if( m_bCanEncodeNextFrame == false )
        {
           PROFILE( "Previously encoded frame not sent\n" );
        }
        else
        {
           PROFILE( "Frame encoding started\n");
           /* Make sure the whole buffer is used for input */
           BufferGfx_resetDimensions(lastFrame);
           if( m_nFrameNumber++ % 30 == 0 )
           {
              /*  To generate IDR frame every 30 frames */
              int32_t status = 0;
              VIDENC1_Status encStatus;
              encStatus.data.buf = NULL;
              encStatus.size = sizeof( VIDENC1_Status );
              /* Generate SPS/PSS headers */
              m_DynamicEncoderParameters.generateHeader = XDM_GENERATE_HEADER;
              m_DynamicEncoderParameters.forceFrame = IVIDEO_NA_FRAME;
              status = VIDENC1_control(Venc1_getVisaHandle( m_Encoder) , XDM_SETPARAMS, &m_DynamicEncoderParameters, &encStatus);
              if( status != VIDENC1_EOK )
              {
                 LOG() << "ERROR: Failed to set encoder control SPS/PPS headers\n";
              }
              /* Encode the video buffer */
              if (Venc1_process(m_Encoder, lastFrame, m_EncodedFrame) < 0)
              {
                 LOG() << "ERROR: Failed to encode video buffer\n";
              }
              m_DynamicEncoderParameters.generateHeader = XDM_ENCODE_AU;
              m_DynamicEncoderParameters.forceFrame = IVIDEO_IDR_FRAME;
              status = VIDENC1_control( Venc1_getVisaHandle( m_Encoder ), XDM_SETPARAMS, &m_DynamicEncoderParameters, &encStatus );
              if( status != VIDENC1_EOK )
              {
                 LOG() << "ERROR: Failed to set encoder control force IDR frame\n";
              }
              /* Encode the video buffer */
              if (Venc1_process(m_Encoder, lastFrame, m_EncodedFrame) < 0)
              {
                 LOG() << "ERROR: Failed to encode video buffer\n";
              }
              bFrameEncoded = true;
              /* Set next frame to be not a forced keyframe */
              m_DynamicEncoderParameters.generateHeader = XDM_ENCODE_AU;
              m_DynamicEncoderParameters.forceFrame = IVIDEO_NA_FRAME;
              status = VIDENC1_control(Venc1_getVisaHandle(m_Encoder), XDM_SETPARAMS, &m_DynamicEncoderParameters, &encStatus);
              if( status != VIDENC1_EOK )
              {
                 LOG() << "Failed to set encoder control\n";
              }
           }
           else
           {
              /* Encode the video buffer */
              if( Venc1_process( m_Encoder, lastFrame, m_EncodedFrame ) < 0 )
              {
                 LOG() << "Failed to encode video buffer\n";
              }
              else
              {
                 bFrameEncoded = true;
              }
         }
        /* if encoder generated output content, free released buffer */
        if( bFrameEncoded )
        {
          PROFILE( "Frame encoding finished\n");
        	if( Buffer_getNumBytesUsed( m_EncodedFrame ) > 0 )
			{
               /* Get free buffer */
               Buffer_Handle hFreeBuf = Venc1_getFreeBuf(m_Encoder);
               /* Free buffer */
               BufTab_freeBuf(hFreeBuf);
             	m_bHasAvailableFrame = true;
             	m_bCanEncodeNextFrame = false;
             	{
             	   posix::AutoMutexLocker oLocker( m_ListenerMutex );
             	   if( m_pListener )
             	   {
             		   m_pListener->OnFrameEncoded();
                   PROFILE( "Notified about the encoded frame\n");
             	   }
             	}

            }
            /* if encoder did not generate output content */
            else
            {
               LOG() << "ERROR: Encoder generated 0 size frame\n";
            }
           
      }
      

        }
 UInt32 time;
          Time_delta(hTime, &time);
          if(time < 40000)  
      {
        PROFILE("Encoder thread is sleeping " << static_cast< int >( 40000 - time ) << "us\n" );
        usleep(40000 - time);  /*  40ms which is 25 fps  */
      }
      Time_reset(hTime);
   	  
   }
   TRACE( "leave\n" );
   return EXIT_SUCCESS;
}

void EncoderWorker::SetListener( EncoderEventListener *pListener )
{
   posix::AutoMutexLocker oLocker( m_ListenerMutex );
   m_pListener = pListener;
}

bool EncoderWorker::IsEncodedFrameAvailable()
{
   return m_bHasAvailableFrame;
}

void EncoderWorker::GetEncodedFrame( unsigned char* rpBuffer, size_t &rnUsedBufferSize, size_t &rnActualBufferSize )
{
   TRACE( "enter\n" );
   unsigned char *pFrameBuffer = reinterpret_cast< unsigned char * >( Buffer_getUserPtr( m_EncodedFrame ) );
   size_t nFrameBufferSize = Buffer_getNumBytesUsed( m_EncodedFrame );
   /* Skip MPEG start code 00 00 00 01 */
   nFrameBufferSize -= 4;
   rnActualBufferSize = nFrameBufferSize;
   if( nFrameBufferSize > rnUsedBufferSize)
   {
      LOG() << "Frame too big for one packet\n";
      nFrameBufferSize = rnUsedBufferSize;
   }
   else
   {
      rnUsedBufferSize = nFrameBufferSize;
   }
   memmove( rpBuffer, &pFrameBuffer[ 4 ], nFrameBufferSize );
   m_bHasAvailableFrame = false;
   m_bCanEncodeNextFrame = true;
   PROFILE( "Encoded frame has been given away, can encode new frame now\n" );
   TRACE( "leave\n" );   
}