#include "EncodedCameraStream.hpp"
#include "Common.hpp"

#include <xdc/std.h>

#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/CERuntime.h>

#include <ti/sdo/dmai/Dmai.h>
#include <ti/sdo/dmai/Ccv.h>
#include <ti/sdo/dmai/Cpu.h>
#include <ti/sdo/dmai/Time.h>
#include <ti/sdo/dmai/BufTab.h>
#include <ti/sdo/dmai/Framecopy.h>
#include <ti/sdo/dmai/BufferGfx.h>

#include <algorithm>

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>

/* vbuf size that has been selected based on size/performance tradeoff */
#define VBUFSIZE                20480

#define VIDEO_BUFFER_COUNT  3

EncodedCameraStream::EncodedCameraStream() : m_pCameraWorker( NULL ), m_pEncoderWorker( NULL ), m_Engine( NULL ), m_SharedBuffers( NULL ), m_bFirstRun( true )
{
   TRACE( "enter\n" );
  VIDENC1_Params parameters = Venc1_Params_DEFAULT;
  BufferGfx_Attrs gfxAttrs = BufferGfx_Attrs_DEFAULT;
  ColorSpace_Type colorSpace = ColorSpace_YUV420PSEMI;
  Capture_Attrs  cAttrs = Capture_Attrs_DM365_DEFAULT;
  VideoStd_Type videoStd = VideoStd_720P_60;
  Int32 width, height, lineLength, bufSize;
  cAttrs.videoInput = Capture_Input_CAMERA;
    
    if (VideoStd_getResolution(videoStd, &width, &height) < 0) {
       LOG() << "ERROR: Failed to detect input resolution\n";
       return;
    }
    
    /*  To solve multiple 16 for encoder input buffer for 1080p */
    /*  The input buffer width & height for encoder MUST multiple 16  */
    if(width % 16 != 0)
      width += (width % 16);
    
    if(height % 16 != 0)
      height += (height % 16);

    lineLength = BufferGfx_calcLineLength(width, colorSpace);
    if (lineLength < 0) {
        LOG() << "ERROR: Failed to calculate input line length\n";
        return;
    }

    bufSize = lineLength * height * 3 / 2;

    /* Calculate the dimensions of the video standard */
    if (BufferGfx_calcDimensions(videoStd, colorSpace, &gfxAttrs.dim) < 0) {
        LOG() << "ERROR: Failed to calculate dimensions for video driver buffers\n";
        return;
    }

    gfxAttrs.colorSpace = colorSpace;
    /*  To solve multiple 16 for encoder input buffer for 1080p */
    gfxAttrs.dim.width = width;
    gfxAttrs.dim.height = height;

    /* Create a table of buffers to use with the capture and display drivers */
    /*  The input buffer width & height for encoder MUST multiple 16  */
    m_SharedBuffers = BufTab_create(VIDEO_BUFFER_COUNT, bufSize, BufferGfx_getBufferAttrs(&gfxAttrs));
    if (m_SharedBuffers == NULL) {
        LOG() << "ERROR: Failed to allocate contiguous buffers\n";
        return;
    }

    /*  Get correct resolution again for 1080p */
    if (VideoStd_getResolution(videoStd, &width, &height) < 0) {
        LOG() << "ERROR: Failed to get video resolution after size adjustments\n";
        return;
    }
    
    /* Create the capture display driver instance */
    cAttrs.numBufs          = VIDEO_BUFFER_COUNT;
    cAttrs.captureDimension = &gfxAttrs.dim;
    cAttrs.colorSpace       = colorSpace;
    cAttrs.videoInput       = Capture_Input_CAMERA;
    cAttrs.videoStd         = videoStd;
    m_pCameraWorker.reset( new CameraWorker( m_SharedBuffers, cAttrs ) );
    m_pCameraWorker->SetListener( this );
        
    /* Open the codec engine */
    char engine[7];
    strcpy(engine, "encode");
    m_Engine = Engine_open(engine, NULL, NULL);

    if (m_Engine == NULL) {
        LOG() << "ERROR: Failed to open encoder codec engine\n";
        return;
    }

    parameters.rateControlPreset = IVIDEO_LOW_DELAY;
    parameters.maxBitRate        = 2000000;
    parameters.inputChromaFormat = XDM_YUV_420SP;
    parameters.reconChromaFormat = XDM_YUV_420SP;
 
    parameters.maxWidth              = width;
    parameters.maxHeight             = height;
    parameters.maxInterFrameInterval = 1;
    m_pEncoderWorker.reset( new EncoderWorker( m_Engine, *m_pCameraWorker.get(), m_SharedBuffers, parameters ) );
    m_pEncoderWorker->SetListener( this );
   TRACE( "leave\n" );
}

EncodedCameraStream::~EncodedCameraStream()
{
   TRACE( "enter\n" );
        if( m_pCameraWorker.get() != NULL )  
        {
           m_pCameraWorker->SetListener(NULL);
           m_pCameraWorker->Stop();
        }
        if( m_pEncoderWorker.get() != NULL )
        {
           m_pEncoderWorker->SetListener(NULL);     
           m_pEncoderWorker->Stop();
        }
        if (m_Engine) 
        {
            Engine_close(m_Engine);
        }
        if( m_SharedBuffers )
        {
            BufTab_delete( m_SharedBuffers );
        }

   TRACE( "leave\n" );
}

bool EncodedCameraStream::IsEncodedFrameAvailable()
{
   TRACE( "enter\n");
   return m_pEncoderWorker->IsEncodedFrameAvailable();
   TRACE( "leave\n" );  
}

void EncodedCameraStream::GetEncodedFrame( unsigned char* rpBuffer, size_t &rnUsedBufferSize, size_t &rnActualBufferSize )
{
   TRACE( "enter\n");
   m_pEncoderWorker->GetEncodedFrame(rpBuffer, rnUsedBufferSize, rnActualBufferSize);
   TRACE( "leave\n" );
}

void EncodedCameraStream::AddListener( StreamEventListner *pListener )
{
   TRACE( "enter\n");
   posix::AutoMutexLocker Locker( m_ListenerMutex );
   m_Listeners.push_back( pListener );
   LOG() << "Stream listener count: " << m_Listeners.size() << "\n";
   if( m_Listeners.size() == 1 )
   {
      if( m_bFirstRun )
      {
         m_pCameraWorker->Start();
         m_bFirstRun = false;
      }
      else
      {
         LOG() << "Resuming camera & encoder\n";
         m_pCameraWorker->Resume();
         m_pEncoderWorker->Resume();
      }
   }
   TRACE( "leave\n" );  
}

void EncodedCameraStream::RemoveListener( StreamEventListner *pListener )
{
   TRACE( "enter\n");
   posix::AutoMutexLocker Locker( m_ListenerMutex );
   std::vector< StreamEventListner* >::iterator ListenerIterator = std::find( m_Listeners.begin(), m_Listeners.end(), pListener );
   if( ListenerIterator != m_Listeners.end() )
   {
      m_Listeners.erase( ListenerIterator );
   }
   LOG() << "Stream listener count: " << m_Listeners.size() << "\n";
   if( m_Listeners.size() == 0 )
   {
      LOG() << "Suspending camera & encoder\n";
      m_pEncoderWorker->Pause();
      m_pCameraWorker->Pause();
   }
   TRACE( "leave\n" );
}

void EncodedCameraStream::OnFrameEncoded()
{
   TRACE( "enter\n");
   posix::AutoMutexLocker Locker( m_ListenerMutex );
   for( std::vector< StreamEventListner* >::iterator ListenerIterator = m_Listeners.begin(); ListenerIterator != m_Listeners.end(); ++ListenerIterator )
   {
      ( *ListenerIterator )->OnFrameReady();
   }
   TRACE( "leave\n" );  
}

void EncodedCameraStream::OnFirstFrameReceived()
{
   TRACE( "enter\n");
   m_pEncoderWorker->Start();
   TRACE( "leave\n" );  
}
