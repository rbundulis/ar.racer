#pragma once 

#include <tr1/memory>

template< typename T >
class Singleton 
{
public:
	static std::tr1::shared_ptr< T > GetInstance()
	{
		static std::tr1::weak_ptr< T > s_pT;
		std::tr1::shared_ptr< T > pT = s_pT.lock();
		if( pT.get() == NULL )
		{
			pT.reset( new T() );
			s_pT = pT;
		}
		return pT;
	}

protected:
	Singleton() {}
	virtual ~Singleton() {}
};