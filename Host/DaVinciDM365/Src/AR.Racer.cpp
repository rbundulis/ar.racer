#include "Common.hpp"
#include "H264EncodedDM365CameraMediaSubsession.hpp"
#include "EncodedCameraStream.hpp"

#include <RTSPServer.hh>

#include <xdc/std.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/dmai/Dmai.h>

#include <signal.h>
#include <execinfo.h>

#include <cstdlib>
#include <memory>
#include <iostream>

void SignalHandler( int nSignalNumber, siginfo_t *pSignalInfo, void *pContext );

int main(int argc, char** argv)
{
   /* Setup crash handling */
   struct sigaction signalAction;
   signalAction.sa_sigaction = SignalHandler;
   signalAction.sa_flags = SA_RESTART | SA_SIGINFO;
   if( sigaction( SIGSEGV, &signalAction, reinterpret_cast< struct sigaction * >( NULL ) ) != 0 )
   {
      std::cerr << "Could not setup signal handlers!" << std::endl;
   };
   CERuntime_init();
   Dmai_init();
   std::auto_ptr< TaskScheduler > pScheduler( BasicTaskScheduler::createNew() );
   BasicUsageEnvironment *pEnvironment =  BasicUsageEnvironment::createNew( *pScheduler );
   {
      INIT_LOG();
      SET_LOG_SINK( pEnvironment );
      std::tr1::shared_ptr< EncodedCameraStream > pEncodedCameraStream = EncodedCameraStream::GetInstance();
      RTSPServer *pRtspServer( RTSPServer::createNew( *pEnvironment, 554, NULL ) );
      if( pRtspServer == NULL )
      {
         LOG() << "ERROR: Failed to create RTSP server: " << pEnvironment->getResultMsg() << "\n";
         exit( 1 );
      }
      LOG() << "RTSP server started\n";
      const char* pStreamName = "camera_h264";
      const char* pDscriptionString = "H264 encoded camera output stream";
      std::auto_ptr< ServerMediaSession > pRtspSession( ServerMediaSession::createNew( *pEnvironment, pStreamName, pStreamName, pDscriptionString ) );
      pRtspSession->addSubsession( H264EncodedDM365CameraMediaSubsession::createNew( *pEnvironment ) );
      pRtspServer->addServerMediaSession( pRtspSession.get() );
      pEnvironment->taskScheduler().doEventLoop();
      SET_LOG_SINK( NULL );
   }
   return EXIT_SUCCESS;
}

void SignalHandler( int nSignalNumber, siginfo_t *pSignalInfo, void *pContext )
{
  void *pBacktraceBuffer[ 50 ];
  size_t nBacktraceSize = backtrace( pBacktraceBuffer, sizeof( pBacktraceBuffer ) ); 
  std::cerr << "Error: signal "  << nSignalNumber << std::endl;
  backtrace_symbols_fd( pBacktraceBuffer, nBacktraceSize, 2 );
  exit( EXIT_FAILURE );
}